string_changes = {
    'Enfants Du Mekon': 'Enfants du Mékong',
    'Rest ': 'Restaurant'
}

compound_names = ('Super U', 'E.Leclerc', 'Leader Price', 'G20Supermarch', 'B Bim', '7-Eleven', "P'tite", 'C&A',
                  'Hot Dogs', "Carl's Jr.", 'Del Arte', 'Laser Quest', 'Top-Up', 'Enfants du Mékong')

categories = {
    'groceries': {
        'groceries': {'Tesco', 'Asda', 'Aldi', 'Lidl', 'Franprix', 'Monoprix', 'Auchan', 'Spar', 'SuperU', 'ELeclerc',
                      'LeaderPrice', 'Intermarch', 'Carrefour', 'Action', 'GTwentySupermarch', 'Walgreens', 'BBim'}
    },
    'restaurant': {
        'sandwich': {'Subway', 'SevenEleven', 'croissant'},
        'traiteur': {'Selecta'},
        'bakery': {'Fournil', 'crumb', 'brioche'},
        'burger': {'HotDogs', 'Quick', 'CarlsJr', 'McDonald'},
        'chicken': {'KFC', 'Popeyes'},
        'crepe': {'Creperie', 'Suzette'},
        'snack': {'Coffee', 'Relay'},
        'pizza': {'DelArte', 'Pizzeria', 'dominos'},
        'mexican': {'Taco', 'Burrito', 'WetWendySBar'},
        'middle east': {'Istanbul', 'Turc'},
        'restaurant': set(),
        'fast food': {'Bagelstein', 'Deliveroo'}},
    'transportation': {
        'public transports': {'Trams', 'Buses', 'RATP', 'Tisseo', 'Superpedestrian', 'RTM',
                              'Tisséo', 'Transdev', 'Sivom'},
        'plane': {'Ryanair', 'EasyJet', 'airport'},
        'automobile': {'Sanef', 'Cofiroute', 'fines', 'highways', 'TotalEnergies', 'Tunnel'},
        'train': {'Trainline', 'SNCF'},
        'carpooling': {'BlaBlaCar', }},
    'divertissement': {
        'bar': {'Pub', 'Beer'},
        'associative': {'helloasso', 'leetchi', 'Ulule', 'EnfantsDuMekon'},
        'recreational': {'kart', 'LaserQuest'},
        'cinema': {'Gaumont', }},
    'health': {
        'pharmacie': set(),
        'consultation': {'Doctolib', 'doctor'},
        'operation': {'Radio', }},
    'shopping': {
        'clothing': {'Teeshirt', 'Celio', 'CandA', 'Jules'},
        'miscellaneous': {'Photomaton', 'Amazon', 'Carrick', 'Print', 'Steam', 'Fnac', 'Boulanger', 'Decathlon', 'OVH',
                          'post', 'GiFi', 'Service'}},
    'culture': {
        'monument': {'Basilica',}},
    'money transferts': {
        'top-up': {'TopUp', 'Payment'},
        'cash flow': {'exchang', 'cash'},
        'salary': {'salaire',}},
    'housing':
        {'hotel': {'Booking',}},
    'education':
        {'school': {'tution', 'Visa'}},
}
