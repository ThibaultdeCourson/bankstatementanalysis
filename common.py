# Base modules
import numpy as np
from os.path import isfile
import re
from datetime import datetime, date, time
from typing import Tuple
import pandas as pd

from exchange_rates import get_exchange_rates

type_patterns = {'Card payment': (r'(REMBOURST CB) DU', r'(FACTURE\(S\))(?: DU|)', r'^DU'),
                 'Cash': (r'(RETRAIT DAB)',),
                 'Transfer': (r'(VIRT? SEPA RECU)', r'(VIRT? SEPA EMIS)', r'(VIREMENT SEPA)',
                              r'(VIR EUROPEEN SEPA)', r'(VIR INST)', r'(VIR SCT INST)'),
                 # , r'^ANN PRLV SEPA EMIS '
                 'Internal transfer': (r'VIRT? CPTE A CPTE EMIS', r'VIRT? CPTE A CPTE RECU', '^(Initial balance)'),
                 'Standing order': (r'(PRLV SEPA)', r'(PRLV EUROPEEN SEPA)', r'(PRELEVEMENT)',
                                    r'(VIREMENT (?:RECU|FAVEUR) TIERS(?: VR.PERMANENT|))'),
                 'Electronic payment': (r'(TELEREGLEMENT)',),
                 'Deposit': (r"(VERSEMENT D'ESPECES)",),
                 'Bank fee': (r'(FRAIS DE GESTION)', r'\*(INTERETS DEBITEURS)',
                              r'\*(COMMISSIONS(?: COTISATION(?: A UNE OFFRE GROUPEE DE SERVICES|)|))',
                              r'(TIRAGE DE CHEQUES)', r'(COTISATION PROVISIO)')}


def year_number_to_pattern(year: int) -> str:
    """
    Returns a regex pattern matching a year number
    :param year: year number
    :return: regex pattern
    """
    return str(year - (1900 if year < 2000 else 2000)) + '|' + str(year)


def number_to_pattern(number: int) -> str:
    """
    Returns a regex pattern for a number
    :param number: number
    :return: regex pattern
    """
    return ('0?' if number < 10 else '') + str(number)


def get_correct_day_range_pattern(year: int, month: int) -> str:
    """
    Returns a regex pattern matching a day number based on the number of days in the month and the year
    :param year: year number
    :param month: month number
    :return: regex pattern
    """

    is_leap_year = (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0)
    if month in {1, 3, 5, 7, 8, 10, 12}:  # Jan, Mar, May, Jul, Aug, Oct, Dec
        return r'0?[1-9]|[12][0-9]|3[01]'
    elif month in {4, 6, 9, 11}:  # Apr, Jun, Sep, Nov
        return r'0?[1-9]|[12][0-9]|30'
    elif month == 2:  # Feb
        return r'0?[1-9]|1[0-9]|2[0-9]' if is_leap_year else r'0?[1-9]|1[0-9]|2[0-8]'
    else:
        raise Warning('Incorrect month number (' + str(month) + ')')


def date_pattern_from_month_year(month: int, year: int) -> Tuple[str, str, str]:
    """
    Returns three regex pattern matching respectively a day, a month and a year numbers
    :param month: month number
    :param year: year number
    :return: a day, month and year pattern (taking into account the number of days in the month)
    """

    if month == 0:
        month = 12
        year -= 1
    day = get_correct_day_range_pattern(year, month)
    month = number_to_pattern(month)
    year = year_number_to_pattern(year)

    return day, month, year


def process_BNPP_description(row: pd.Series) -> dict[str,]:
    """
    Attribute a category to a transaction based on its description

    :param row: a Series containing a string value in a 'Description' cell
    :return: a Series containing two values: a status (COMPLETED or REVERTED) and a category
    """

    description = row['Description']
    tags = {'State': 'COMPLETED'}  # List of tags with their value

    # Detect reverted operations
    if re.match(r'^ANN ', description):
        description = description[4:]
        tags['State'] = 'REVERTED'

    date_pattern = r' (\d{2})/(\d{1,2})/(\d{1,2})'
    card_pattern = r'(?:000)?\d{4}X{8}(\d{4})'
    end_pattern = r'(?: |$)'

    # Search for BNPP's patterns
    if match := re.match(r'^(ECHEANCE PRET) (\d+ \d+) \(CAPITAL DU (\d+),(\d{2}) EUR\)' + end_pattern, description):
        tags.update({'Original type': 'Echéance prêt',
                     'Type': 'Standing order',
                     'Ref': match.group(2),
                     'Capital': float(match.group(3) + '.' + match.group(4))})
        description = description[match.end(0):]

    elif match := re.match(r'^CHEQUE (\d+)' + end_pattern, description):
        tags.update({'Original type': 'Chèque',
                     'Type': 'Check',
                     'Ref': int(match.group(1))
                     })
        description = description[match.end(0):]

    elif match := re.match(r'^RETRAIT DAB ' + date_pattern + r'(?:\*|\d{1,2}H\d{2}) ([a-zA-Z0-9]+)' + end_pattern,
                           description):

        tags.update({'Original type': 'Retrait distributeur',
                     'Type': 'Cash',
                     'Ref': match.group(4)
                     })
        description = description[match.start(1):]

    elif match := re.match(r'^VIRT? CPTE A CPTE EMIS SUR LE (\w+)' + end_pattern, description):
        tags.update({'Original type': 'Virement compte à compte',
                     'Type': 'Internal transfer',
                     'To': match.group(1)
                     })
        description = description[match.end(0):]

    elif match := re.match(r'^REMISE CHEQUES BORDEREAU (\d+)' + end_pattern, description):
        tags.update({'Original type': 'Remise chèques',
                     'Type': 'Check',
                     'Ref': match.group(1)
                     })
        description = description[match.end(0):]

    else:
        previous = -1
        for type_pattern in type_patterns:
            for pattern in type_patterns[type_pattern]:

                # If the pattern is found in the description
                temp = ''
                for match in re.finditer(pattern + end_pattern, description):

                    original_type = None if match.lastindex is None else match.group(1).capitalize()

                    # If a pattern is found after a first pattern identification
                    if 'Original type' in tags:
                        if original_type == tags['Original type']:
                            temp += description[previous:match.start(0)]
                            previous = match.end(0)
                        else:
                            print("WARNING: Two type patterns matching:", pattern)
                    else:
                        tags.update({'Type': type_pattern,  # Attribute the type to the description
                                     'Original type': original_type
                                     })
                        previous = match.end(0)

                if previous != -1:
                    description = temp + description[previous:]
                    previous = -1

    # If no pattern is found in the description
    if 'Type' not in tags:
        print("WARNING: type not found for [", description, "]", sep='')

    if tags['Original type'] is not None and re.search(r'\brecu\b', tags['Original type']):
        tags['Ben'] = "Me"
    elif tags['Original type'] is not None and re.search(r'\bemi\b', tags['Original type']):
        tags['De'] = "Me"

    # Search for a credit card pattern ("CARTE 0000XXXXXXXX0000" or "REVOLUT*0000*")
    while card_match := re.search(r'(?:^|\s)((?:CARTE )?' + card_pattern + r'|REVOLUT\*{1,2}(\d{4})\*)(?:\s|$)',
                                  description):

        # Remove the credit card information from the description
        description = description[:card_match.start(1)] + description[card_match.end(1):]

        # Add the credit card information to the list of tags
        if 'Card' in tags:
            #print('[', description, ']', sep='')
            if 'Card2' in tags:
                raise Warning('Three cards identified in the description: ' + tags['Card'] + ', ' + tags['Card2'] +
                              ' and ' + get_not_null_re_group(card_match, 2) +
                              '\nDescription:' + row['Description'])
            tags['Card2'] = get_not_null_re_group(card_match, 2)
        else:
            #print('REMOVING CARD(S):\n[', row['Description'], ']\n[', description, ']', sep='')
            tags['Card'] = get_not_null_re_group(card_match, 2)

    # Search for date pattern
    year, transaction_time = None, None
    current_year = row['Completed Date'].year
    current_month = row['Completed Date'].month

    separator = r'[\/\-\.\s]*'
    any_month = r'0[1-9]|1[0-2]'
    any_day = r'0[1-9]|[1-2][0-9]|3[0-1]'
    found = True
    anormal_date = False
    description = pd.DataFrame(data={'str': description, 'check': True}, index=[0])
    preceding_pattern = r'(?<!\d[\/\.\-\s])(?<!\d)'


    while found:

        found = False
        for i, description_segment in description.iterrows():
            string = description_segment['str']
            to_check = description_segment['check']
            if to_check:
                for day_pattern, month_pattern, year_pattern in (
                        (number_to_pattern(row['Completed Date'].day), number_to_pattern(current_month),
                         year_number_to_pattern(current_year)),  # Search for the transaction date
                        date_pattern_from_month_year(current_month, current_year),
                        # Search for a date of the current month
                        date_pattern_from_month_year(current_month - 1, current_year),
                        # Search for a date of the past month
                        (any_day, any_month, year_number_to_pattern(current_year)),
                        # Search for a date of the current year
                        (any_day, any_month, r'19\d{2}|20\d{2}|\d{2}')
                ):

                    if date_match := re.search(
                            preceding_pattern + r'(' + year_pattern + r')' + separator + r'(' + month_pattern + r')(?:' + separator + r'(' +
                            day_pattern + r'))?(?:\D|$)', string):

                        year = date_match.group(1)
                        month = date_match.group(2)
                        day = date_match.group(3)
                        if day is None:
                            date_start = date_match.start(1)
                            date_end = date_match.end(2)
                        found = True

                    elif date_match := re.search(
                            preceding_pattern + r'(?:(' + day_pattern + r')' + separator + r')?(' + month_pattern + r')' + separator + r'(' +
                            year_pattern + r')(?:\D|$)', string):

                        year = date_match.group(3)
                        month = date_match.group(2)
                        day = date_match.group(1)
                        if day is None:
                            date_start = date_match.start(2)
                            date_end = date_match.end(3)
                        found = True

                    if found:
                        break

                if found:
                    break
                else:
                    description.loc[i, 'check'] = False

        if found:
            year = int(year)
            if year < 100:
                if year < date.today().year - 1998:
                    year += 2000
                else:
                    year += 1900

            if day is None or re.search(get_correct_day_range_pattern(year, int(month)), day):

                transaction_date = month + '/' + str(year)
                if day is not None:
                    transaction_date = day + '/' + transaction_date
                    t_date = date(year, int(month), int(day))
                else:
                    t_date = date(year, int(month), 1)
                date_distances = {'start': None if row['Started Date'] is None else (
                            t_date - row['Started Date'].to_pydatetime().date()).days,
                                  'end': (t_date - row['Completed Date'].to_pydatetime().date()).days}
                if (date_distances['start'] is None or abs(date_distances['start']) > 31*2) and abs(
                        date_distances['end']) > 31*2:
                    anormal_date = True

                if day is not None:
                    date_start = date_match.start(1)

                    if not anormal_date and (
                    time_match := re.match(r'^ (\d{1,2})H([0-5]\d)' + end_pattern, string[date_match.end(3):])):
                        # If time information specified add it
                        transaction_date = time_match.group(1) + 'h' + time_match.group(2) + ' ' + transaction_date
                        date_end = time_match.end(2) + date_match.end(3)
                    else:
                        date_end = date_match.end(3)

                if anormal_date:
                    """
                    print("ANORMAL DATE: ", string[date_start:date_end], ' (', transaction_date,
                          ", distance to transaction dates: ", date_distances['start'], ' and ', date_distances['end'], ')', sep='')
                    """
                    transaction_date = string[date_start:date_end]

                if not anormal_date and date_start > 3 and re.search(r'(?:\s|^)sal',
                                                                     string[date_start - 4:date_start].lower()):
                    beginning = string[:date_start - 3] + 'salaire '
                elif not anormal_date and date_start > 7 and re.search(r'(?:\s|^)salaire',
                                                                       string[date_start - 8:date_start].lower()):
                    beginning = string[:date_start] + ' '
                else:
                    beginning = string[:date_start]

                    if not anormal_date and len(beginning) > 0 and beginning[-1] != ' ':
                        beginning += ' '

                ending = string[date_end:]
                if not anormal_date and len(ending) > 0 and ending[0] != ' ':
                    ending = ' ' + ending

                description = pd.concat([description.iloc[:i],
                                         pd.DataFrame({'str': beginning, 'check': len(beginning) > 6 and bool(re.search(r'\d{4,}', beginning))}, index=[0]),
                                         pd.DataFrame({'str': transaction_date, 'check': False}, index=[0]),
                                         pd.DataFrame({'str': ending, 'check': len(ending) > 6 and bool(re.search(r'\d{4,}', ending))}, index=[0]),
                                         description.iloc[i + 1:]
                                         ], ignore_index=True, axis=0)

    """
    if len(description) > 3:
        print("Multiple dates detected:\n", row['Description'], '\n', ''.join(description['str']))
    """
    description = ''.join(description['str'])

    to_process_start = 0  # Set the cursor to the beginning of the string

    # Catch the next occurrence of a tag ('/XXX' or 'XXX/')
    next_tag1 = r'/[A-Z]{1,10}(?:$|\s)'
    next_tag2 = r'[A-Z]{1,10}/(?:$|.*)'
    #               2 & 4: value of the tag
    filling = r'(?:(.*?)|)' + r'(?:\s' + next_tag1 + r'|\s' + next_tag2 + '|$)'
    #          1: first tag (slash before option)
    tag1 = r'/([A-Z]{1,10})' + r'(?:$|\s' + filling + ')'
    #         3: first tag (slash after option)
    tag2 = r'([A-Z]{1,10})/' + r'(?:$|' + filling + ')'
    #           0
    pattern = r'(?:^|\s)(?:' + tag1 + '|' + tag2 + ')'

    def add_to_string_var(var, col, value):
        if col in tags and tags[col] is not None:
            var[col] += ' ' + value
        else:
            var[col] = value
        return var

    # Iterate on the tags found in the description
    while tag_match := re.search(pattern, description[to_process_start:]):

        tag = get_not_null_re_group(tag_match, 1, 2).capitalize()

        # Get the value of the tag detected
        value_index = get_not_null_re_group_idx(tag_match, 2, 2)
        value = tag_match.group(value_index)

        if to_process_start == 0 and tag_match.start(0) != 0:
            tags = add_to_string_var(tags, 'Motif', description[:tag_match.start(0)])

        # Indicate where to pick off
        to_process_start += (
            # If the tag has no value, use the end of the key
            tag_match.end(get_not_null_re_group_idx(tag_match, 1, 2)) + 1
            if value is None else
            # If the tag has a value, use the end of the value
            tag_match.end(value_index)
        )

        if value == 'NOTPROVIDED':
            value = None

        if tag != 'Motif' and tag in tags:
            raise Warning('Tag [' + tag + '] overwritten: [' + str(tags[tag]) + '] replaced by [' + str(value) + ']')
        if tag == 'Du':
            tags = add_to_string_var(tags, 'Motif', 'du' + value)
        else:
            tags[tag] = value

    # If no tag found, save the whole string in a Motif tag
    if to_process_start == 0:
        tags = add_to_string_var(tags, 'Motif', description)

    # return pd.Series([status, detected_type])
    return tags  # Return the dictionary of tags


def import_txt_statement(filename, replacements, initial_balance=None):
    if isfile(filename):

        # Open the file and read its content.
        with open(filename, encoding='utf-8') as f:

            data = []

            # skip useless header
            while not (line := re.match(r"^SOLDE\s+.*(\d{2})\.(\d{2})\.20(\d{2})(\s{95,})(([0-9\.\ \,]{3,}))",
                                        f.readline().strip())):
                r"""
                r" - Indicates a raw string literal, where special characters are treated literally (the backslashes are not escape characters)
                ^ - Asserts the position at the start of a line
                SOLDE - The literal string "SOLDE"
                \s+ - One or more whitespace characters (like spaces or tabs)
                .* - Any character (except for newline characters) zero or more times
                \d{2} - Two digits (0-9)
                \. - Literal period/dot (.)
                \d{2} - Two digits (0-9).
                \.20 - The literal string ".20"
                (\d{2}) - Two digits (0-9). The parentheses () indicate a capturing group, which allows to extract the matched digits separately
                """
                # print("[", line, "]", sep='')
                pass
            start_date_year = completed_date_year = int('20' + line.group(3))
            start_date_month = completed_date_month = int(line.group(2))

            balance = float(
                line.group(5).replace('.', '').replace(',', '.').replace(' ', ''))
            if len(line.group(4)) < 108:
                balance *= -1
            if initial_balance is None:
                data.append({
                    "Started Date": None,
                    'Completed Date': datetime(year=completed_date_year,
                                               month=completed_date_month,
                                               day=int(line.group(1))),
                    "Description": 'Initial balance',
                    "Amount": balance
                })
            elif initial_balance != balance:
                print("WARNING: Incorrect balance generated by ", start_date_month, "/20" + line.group(3), ": ",
                      initial_balance, " instead of ", balance, sep='')

            end = False

            last_amount_scraped = None

            page = 1
            inside_table = True
            n_empty_lines = 0

            # here is the real data
            while True:
                line = f.readline().strip()
                # if start_date_year == 2022 and completed_date_month >= 11 and len(data) >= 8:
                # print("[", line, "]", sep='')

                # stop after this line
                if re.match(r"^TOTAL\s+DES\s+(MONTANTS|OPERATIONS)", line):
                    end = True

                if not end:

                    if inside_table:

                        # skip empty lines
                        if line == "":
                            n_empty_lines += 1

                            if n_empty_lines > 3:
                                inside_table = False
                                n_empty_lines = 0
                            continue
                        elif n_empty_lines > 0:
                            n_empty_lines = 0

                        # check for page layout patterns
                        pass_line = False
                        for replacement in replacements:
                            if re.match(replacement, line):
                                inside_table = False
                                pass_line = True
                                break
                        if pass_line:
                            continue

                    # Check for the start of a page table pattern
                    else:
                        if re.match(r"^D\s{0,}ate\s+N\s{0,}ature", line):
                            page += 1
                            inside_table = True
                        continue

                    # remove spaces in middle of date and amount
                    # temp_line = re.sub(r"([0-9]+)\s*\.\s*([0-9]+)", r"\1.\2", line)
                    # temp_line = re.sub(r"([0-9\.]+)\s*,\s*([0-9]+)", r"\1,\2", temp_line)
                    # if temp_line != line:
                    #     print("MODIFICATION:\n", line, "\n", temp_line)
                    # line = temp_line

                # if this_month == '05' and year == 2019:
                #             print()

                # Search for a line of format: (Date)  (Description)  (Value)  (Debit)  (Credit)
                if match := re.match(
                        r"^(\d{2})\.(\d{2})\s{2,}((\S|\s{1,29}\S)+)(\s{"
                        + ("7" if page == 2 else "10") +
                        r",})(\d{2})\.(\d{2})((\s{2,})([0-9\.\ \,]{3,}))?",
                        line) or end:
                    r"""
                    r" - Start of a raw string literal
                    ^ - Asserts the position at the start of a line
                    ([0-9\.]{5}) - 1st capturing group, the Date:
                        [0-9\.] - Any digit (0-9) or a literal dot (\.).
                        {5} - Specifies that the previous character class should match exactly 5 times

                    \s{2,} - Two or more whitespace characters (like spaces or tabs)

                    ((\S|\s{1,29}\S)+) - 2nd capturing group, the Description, composed of multiple instances of the sequence (\S|\s{1,29}\S) where:
                        \S - Non-whitespace character
                        | - OR
                        \s{1,29}\S - Up to 29 spaces followed by a non-whitespace character

                    \s{30,} - 30 or more whitespace characters

                    ([0-9\.]{5}) - 3rd capturing group, the Value:
                        [0-9\.]{5} - 5 digits or dots

                    ((\s{2,})([0-9\.\,]{3,}))? - Optional 4th capturing group, Credit:
                        (\s{2,}) - Matches two or more whitespace characters.
                        ([0-9\.\,]{3,}) - Matches a sequence of 3 or more digits, dots, or commas.
                    """

                    # if last_amount_scraped is defined, then append the operation to data
                    if last_amount_scraped is not None:
                        # remove dot as thousand separator (1.000 -> 1000)
                        # and use dot as decimal separator (10,00 -> 10.00)
                        last_amount_scraped = float(last_amount_scraped.replace('.', ''
                                                                                ).replace(',', '.'
                                                                                          ).replace(' ', ''))
                        # if the indentation is below the limit, it is a debit
                        # remove multiple spaces in comment

                        balance += last_amount_scraped

                        comment = re.sub(r"\s+", " ", comment.strip())

                        data.append({
                            "Started Date": started_date,
                            'Completed Date': completed_date,
                            "Description": comment,
                            "Amount": last_amount_scraped
                        })

                        # wait next last_amount_scraped
                        last_amount_scraped = None

                    if end:
                        break

                    # increase year if we go from month 12 to month 1
                    if match.group(2) == '01' and start_date_month == 12:
                        # print("YEAR MODIFICATION:", date_operation_month, month, year, filename)
                        start_date_year += 1
                        start_date_month = 1

                    started_date = datetime(start_date_year, start_date_month, int(match.group(1)))

                    comment = match.group(3)

                    # increase year if we go from month 12 to month 1
                    if match.group(7) == '01' and completed_date_month == 12:
                        # print("YEAR MODIFICATION:", date_operation_month, month, year, filename)
                        completed_date_year += 1
                        completed_date_month = 1

                    completed_date = datetime(completed_date_year, int(match.group(7)), int(match.group(6)))

                    # if len(match.groups()) != 9:
                    # if this_month == '10' and year == 2019:
                    #     print('WARNING: MATCHING FAILED')
                    #     for i in range(1, len(match.groups())):
                    #         print(i, ": ", len(match.group(i)), " [", match.group(i), "] ", sep='')
                    #         i += 1
                    #     print("")

                    if match.group(8) is not None:
                        last_amount_scraped = match.group(8)
                        # if this_month == '10' and year == 2019:
                        #     print(len(match.group(7)), " [", match.group(7), "] ", sep='')
                        if len(match.group(8)) < (35 if page == 2 else 45):
                            last_amount_scraped = '-' + last_amount_scraped
                else:
                    # Search for a line of format: (Description)  (Value)
                    if match := re.match(r"^((\S|\s{1,29}\S)+)((\s{30,})([0-9\.\,]{3,}))?", line):
                        r"""
                        ^ - Asserts the position at the start of a line
                        ((\S|\s{1,29}\S)+) - 1rst capturing group, composed of multiple instances of the sequence (\S|\s{1,29}\S) where:
                            \S - Any non-whitespace character.
                            | - OR
                            \s{1,29}\S - Sequence that starts with 1 to 29 whitespace characters (\s) followed by a non-whitespace character (\S)
                        ((\s{30,})([0-9\.\,]{3,}))? - Optional 2nd capturing group:
                            (\s{30,}) - Matches 30 or more whitespace characters.
                            ([0-9\.\,]{3,}) - Matches a sequence of 3 or more characters that can be digits (0-9), dots (.), or commas (,).
                        """
                        comment += " " + match.group(1)
                        if match.group(4) is not None:
                            print("WARNING: This code may need updating")
                            indent = len(match.group(1) + match.group(4) + match.group(5))
                            last_amount_scraped = match.group(5)
                            if indent < 165:
                                last_amount_scraped = '-' + last_amount_scraped
                    else:
                        print("WARNING: line not matching any pattern !! '{}'".format(line))

            # Convert data to DataFrame
            return data, round(balance, 2)
    return None, None


def get_not_null_re_group_idx(val: re.Match, index: int, spread: int = 1) -> int:
    """
    For a regex expression with two alternative groups, returns the index of the second one if the first is null
    :param val: the regex Match object to inspect
    :param index: the index of the first group
    :param spread: the distance between the first and the second group
    :return: an index of a possibly not null group
    """
    return index + spread if val.group(index) is None else index


def get_not_null_re_group(val: re.Match, index: int, spread: int = 1) -> str | None:
    """
    For a regex expression with two alternative groups, returns the second if the first is null
    :param val: the regex Match object to inspect
    :param index: the index of the first group
    :param spread: the distance between the first and the second group
    :return: a possibly not null group
    """
    return val.group(get_not_null_re_group_idx(val, index, spread))


def process_matched_sequences(text):
    # Add a space between a number and its suffix (for ex: 2ND -> 2 ND)
    text = re.sub(r'(^|\s)(\d+)(ER|ST|ND|RD|EME|TH)(\s|$)', r'\1\2 \3\4', text)

    for string in (r'(^|\s)([A-Z]{2,})(\d+)(\s|$)', r'(^|\s)(\d+)([A-Z]{2,})(\s|$)'):
        # Add a space between a word and a number (or vice versa)
        text = re.sub(string, r'\1\2 \3\4', text)

    to_process_start = 0  # Cursor

    # Removes sequences of digits, /, -, or . that are not dates
    while code_match := re.search(r'(?<!\S)([\d\/\-\.]{5,})(?=\s|$)', text[to_process_start:]):
        # print("1 - [", text, "]", sep='')

        if (
                (en_date_match := re.match(
                    r'^(19\d{2}|20\d{2}|\d{2})[\/\-\.]?(0[1-9]|1[0-2])(?:[\/\-\.]?(0[1-9]|[1-2][0-9]|3[0-1]))?$',
                    code_match.group(1))) or
                (fr_date_match := re.match(
                    r'^(?:(0[1-9]|[1-2][0-9]|3[0-1])[\/\-\.]?)?(0[1-9]|1[0-2])[\/\-\.]?(19\d{2}|20\d{2}|\d{2})$',
                    code_match.group(1)))):
            # If the number is a date:

            year = int(en_date_match.group(1) if en_date_match else fr_date_match.group(3))
            day = en_date_match.group(3) if en_date_match else fr_date_match.group(1)
            month = en_date_match.group(2) if en_date_match else fr_date_match.group(2)

            # Check if the day information is included in the number
            if (fr_date_match.group(1) if en_date_match is None else en_date_match.group(3)) is None:
                date_p1 = month
            else:
                date_p1 = day + '/' + month

            # To convert a year from a short format to a long one (ex: 19 -> 2019)
            # if year < 100:
            #     if year <= datetime.now().year - 1999: year += 2000
            #     else: year += 1900

            # Reformate the date
            text = text[:code_match.start()] + ' ' + date_p1 + '/' + str(year) + ' ' + text[code_match.end():]

            to_process_start += code_match.start() + 8 + len(str(year))
        elif re.match(r'^0[1-9]\d{8}$', code_match.group(1)):
            # If the number is a phone number:

            to_process_start += code_match.end()
        else:
            # If the number is a code:

            # text = text[:to_process_start + code_match.start()] + text[to_process_start + code_match.end():]
            # to_process_start += code_match.start()
            to_process_start += code_match.end()

    # Removes codes
    # for pattern in (r'(^|\s)(?=\S*\d)(?=\S*[A-Z])[\dA-Z./-]+($|\s)', r'(?<!\S)\d+/(?=\s|$)',):
    #
    #     while True:
    #
    #         text2 = re.sub(pattern, ' ', text)
    #         if text2 == text:
    #             break
    #         else:
    #             text = text2
    return None if text == '' else " ".join(text.split())


def set_initial_balance(df, origin):
    # Setup initial balance
    df.sort_values('Completed Date', inplace=True, ascending=True)
    first_day = df.iloc[0]

    initial_amount = first_day['Balance' if 'Balance' in df.columns else 'Solde']

    if not np.isnan(initial_amount):
        initial_amount -= first_day['Amount']
        if initial_amount != 0:
            print("The", origin, "account statement start on the", first_day['Completed Date'], "with an balance of",
                  initial_amount)
            df.loc[len(df)] = {'Completed Date': first_day['Completed Date'], 'Description': 'Initial balance',
                               'Amount': initial_amount, 'Type': 'Internal transfer', 'Fee': 0, 'Currency': 'EUR'}
    return df


last_conversion = {'date': None, 'currency': None, 'exchange_rate': None}


def convert_to_euros(data):
    global last_conversion
    amount, currency, date = data
    exchange_rate = None

    if currency != 'EUR':

        if date == last_conversion['date'] and currency == last_conversion['currency']:
            exchange_rate = last_conversion['exchange_rate']
        else:
            exchange_rate = \
                get_exchange_rates(currency, target_currencies=['EUR', ], on_date=date.strftime('%Y-%m-%d'))['EUR']
            last_conversion = {'date': date, 'currency': currency, 'exchange_rate': exchange_rate}

        amount *= exchange_rate

    return amount


# a, b = import_txt_statement('data/BNPP_statements/2024-09.txt', {})
# pd.DataFrame(a)